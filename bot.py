import asyncio
import json
import re

import discord
from discord.ext import commands, tasks
from discord.utils import get as discord_get

from chain_utils import get_message_chain, unpack_chain
from commands import UserCommands, base_prompt

# from events import Events
from logger_default import custom_logger

logger = custom_logger(__name__).logger


intents = discord.Intents.default()
bot = commands.Bot(command_prefix="$", intents=intents)


intents.members = True
intents.reactions = True
intents.message_content = True
intents.guild_messages = True


async def status_loop():
    STATUS_LOOP = 480
    while True:
        await asyncio.sleep(STATUS_LOOP)


bot.remove_command("help")
task_wrapper = {"task": None}


import datetime


@bot.command("clear")
async def clear_text(ctx, *, line_count):
    logger.info(line_count)
    if int(line_count) < 51:
        if ctx.author.guild_permissions.manage_messages:
            await ctx.channel.purge(limit=int(line_count) + 1)
        else:
            await ctx.send("`only admins can delete history` :triumph: ")
    elif int(line_count) >= 50:
        await ctx.send("`please try to delete in batches of 50 or less` :pray:")


@bot.command(aliases=["eraseme"])
# @commands.has_permissions(administrator = True)
async def erase_me(ctx, *, user_at):
    """use this command to erase a user's messages from the guild if they are over a defined age of 7 days"""

    logger.info(user_at)

    try:
        sno_id = re.search(r"\d+", user_at).group()
        logger.info(sno_id)
        # get member from id

        member = await bot.fetch_user(int(sno_id))
        # member = await ctx.guild.fetch_member(int(sno_id))
    except AttributeError:
        logger.info("user not found")
        return

    logger.info(type(member))
    logger.info(f"found member {member.name} in guild {ctx.guild.name}")

    if member:
        for channel in ctx.guild.text_channels:
            try:
                logger.info(
                    f"ERASING USER {member.name} in channel {channel.name} in guild {ctx.guild.name}"
                )
                last_timestamp = await iterate(channel, member)
                logger.info(f"last timestamp: {last_timestamp}")
                while last_timestamp is not None:
                    last_timestamp = await iterate(channel, member, last_timestamp)
                    logger.info(f"last timestamp: {last_timestamp}")
                    await asyncio.sleep(1)
            except Exception as e:
                logger.warning(f"error: {e}")
                continue


class Counter:
    ct = 0


async def iterate(channel, member, last_timestamp=None):
    msg = await discord.utils.get(
        channel.history(limit=200000, oldest_first=True, after=last_timestamp),
        author__name=member.name,
    )

    # if message older than 7 hours, log message.'
    if msg is None:
        return
    aware_dt = datetime.datetime.now(datetime.timezone.utc)
    if abs(aware_dt - msg.created_at).days > 7:
        logger.info(f"{msg.author.name} {msg.created_at} {msg.content}")

        await msg.delete()
        Counter.ct += 1
        logger.info(f"counter: {Counter.ct}")
        return msg.created_at + datetime.timedelta(seconds=1)


@bot.command(aliases=["$", "", "skibidi"])
async def xl_menu(ctx):
    """
    Sends a message with our dropdown containing colours
    and instantiates the Questionnaire modal if a selection is made.
    """
    from modals import DropdownView

    # Create the view containing our dropdown
    view = DropdownView(ctx, bot)

    # Sending a message containing our view
    await ctx.send("", view=view)
    await ctx.message.delete()


@bot.command(aliases=["redact", "destroy", "redacted"])
async def mr_self_destruct(ctx, *, msg=""):
    try:
        time_s = 15
        split_msg = msg.split("-s ")
        logger.warning(msg)
        logger.warning(split_msg)
        if len(split_msg) > 1:
            print(len(split_msg))
            time_s = split_msg[1]

        logger.warning(ctx.message.author)
        logger.warning(ctx.message.clean_content)
        reply = await ctx.message.reply(
            f"`Message will be deleted in {time_s} seconds`"
        )
        await asyncio.sleep(int(time_s))
        await ctx.message.delete()
        await ctx.send(f"`[message from {ctx.message.author} redacted]`")
        await reply.delete()

    except Exception as e:
        logger.warning(time_s)
        logger.warning(ctx.message)
        logger.error(e)
        await ctx.send(e)
        return


@bot.event
async def on_message(message):
    if (
        message.reference is not None
        and message.author.name == "stable-genius"
        and type(message.channel) == discord.channel.TextChannel
        and message.channel.name == "gpt-threads"
    ):
        logger.info("resonding to reply message")
        ctx = await bot.get_context(message)
        chain = await get_message_chain(message)
        logger.info("message chain length")
        logger.info(len(chain))
        chain = unpack_chain(chain).replace("`", "").replace("stable-genius", "Chatbot")
        logger.info("message chain")
        logger.info(chain)
        # prompt = f'{message.author.name:} {message.content} \n'
        chat_command = bot.get_command("chat")
        combined_message = f"{base_prompt} \n {chain} "
        await chat_command(ctx, prompt=combined_message, reply=True)

    else:
        await bot.process_commands(message)


@bot.event
async def on_raw_reaction_add(payload):
    """used for image-favorites channel, and spoilering sensitive image via votes
    when there is a emoji reaction, if it's a channel related to this bot,
    in the reacts ['👁️', '🙏','♻️', '🐝'], then do the appropriate function
    """
    channel = bot.get_channel(payload.channel_id)
    # any of these patterns will be valid for this bot

    reacts = ["👁️", "🙏", "♻️", "👎", "🐝", "💬"]
    if not payload.emoji.name in reacts:
        return

    channel = bot.get_channel(payload.channel_id)
    message = await channel.fetch_message(payload.message_id)
    reaction = discord_get(message.reactions, emoji=payload.emoji.name)
    guild = bot.get_guild(payload.guild_id)
    reaction_count = reaction.count
    reactor = payload.member
    if reactor.bot:
        return
    ctx = await bot.get_context(message)
    logger.info(ctx.channel)
    ctx.message.author = reactor

    if payload.emoji.name == "🐝":
        prompt = message.content

        for match in re.findall("\<(.+?)\>", prompt):
            prompt = prompt.replace(
                f"<{match}>", re.search("^[^:]*", match).group(0) or ""
            )
        prompt = prompt.replace("<", "").replace(">", "")
        logger.info(f"cleaned prompt: {prompt}")

        logger.info(f"running {prompt} from react")

        if ctx.channel.name != "sdxl-horny-jail":
            ctx.message.channel = discord.utils.get(
                bot.get_all_channels(), name="sdxl-sfw"
            )

        await run_prompt(ctx)
        return

    if payload.emoji.name == "👎":
        # delete the message if it contains the name of the reactor

        content = message.clean_content
        logger.info(payload.member.name)
        logger.info(content)
        if payload.member.name in content:
            await message.delete()
        logger.info(message.author.name)

        if message.author.name == "stable-genius":
            if (
                message.reference is not None
                and message.reference.resolved.author.name == payload.member.name
            ):
                await message.reference.resolved.delete()
                await message.delete()

    if payload.emoji.name == "♻️":
        # have the bot add the "♻️" emoji to the message as well
        await message.add_reaction("♻️")

        await run_prompt(ctx)

        return

    if payload.emoji.name == "👁️":
        # edit the post to have a spoiler tag on the attachment
        if reaction.count > 0:
            # x = await message.attachments[0].to_file(spoiler=True)
            x = [await i.to_file(spoiler=True) for i in message.attachments]

            await message.edit(attachments=x)
            logger.info(f"spoiler message {guild}")
            return

    if payload.emoji.name == "🙏":
        # embed the image in the #image-favorites channel
        # or increment the counter if the embed already exists
        logger.info(payload.emoji.name)
        if channel.name == "image-favorites":
            return

        if reaction and reaction.count > 0:
            await get_stars(guild, message, reaction_count)


async def get_stars(guild, message, reaction_count):
    """make a pretty embed in the favorites channel, or update it if it already exists"""

    star_channel = discord_get(guild.channels, name="image-favorites")
    star_channel = discord_get(guild.channels, name="image-favorites")
    if message.attachments:
        attachment_url = message.attachments[0].url
        logger.debug(f"attachment{attachment_url} {guild.name}")
    else:
        logger.debug("post has no attachments!")
        return
    # set up embed
    desc = f"🙏 {reaction_count}\n\n{message.content}\n**[Jump to message]({message.jump_url} )**"
    embed = discord.Embed(description=desc).set_image(url=attachment_url)
    embedded = False

    # async for loop: can't return from it, but break works
    # loop through history of the star channel,
    # look for an existing instance of the favorited image in the favorited channel
    async for starred in star_channel.history(limit=None, oldest_first=False):
        if (starred.author.name == "stable-genius") and starred.embeds:
            if starred.embeds[0].image:
                embed_img_url = starred.embeds[0].image.url
                if embed_img_url == attachment_url:
                    logger.debug("updating exiting embed")
                    # if there is a matching instance, update the embed with the new vote count
                    await starred.edit(embed=embed)
                    embedded = True
                    break
    # if there is no existing favorite, add the embed to the favorite channel
    if not embedded:
        logger.info("new embed")

        await star_channel.send(embed=embed)


from firestore_db import FirestoreInterface
import os


@bot.command(aliases=["gradio", "gradio_set"])
async def set_gradio_url(ctx):
    """set the gradio url"""
    logger.info(ctx.message.content)
    url = ctx.message.content.split()[-1]
    logger.info(url)
    gradio = json.load(open("gradio.json"))
    gradio["url"] = url
    with open("gradio.json", "w") as gradio_file:
        json.dump(gradio, gradio_file)
    logger.info(gradio)

    os.environ["GRADIO_URL"] = url
    await ctx.send(f"`gradio url set to {url}`")


@bot.command()
async def fsq(ctx):
    """get the current queue of messages"""
    logger.info("getting queue")
    queue = await FirestoreInterface(bot).get_all_ctx()
    logger.info(queue)
    await ctx.send(f"`{queue}`")


async def run_prompt(ctx):
    prompt = ctx.message.content
    logger.info(prompt)
    nq = bot.get_command("xl")
    if prompt.startswith("$"):
        key, text = prompt.split(" ", 1)

        prompt = text
    elif "`" in prompt:
        prompt = prompt.split("`")[1]
    else:
        logger.info(f"no valid character in prompt {prompt}")
        return

    prompt = prompt.replace("queued image for", "")
    prompt = prompt.replace("creating image for", "")

    ctx.message.content = prompt
    logger.info(ctx.author)

    await nq(ctx)


# @bot.command()
# async def sort_channels(ctx):
#     '''sorts channels in each category by name'''
#     # vefify that the user has manage channels permissions
#     if not ctx.author.guild_permissions.manage_channels:
#         await ctx.send('`you do not have permission to do that`')
#         return
#     guild = ctx.guild
#     categories = guild.categories
#     for category in categories:
#         channels = category.channels
#         channels = sorted(channels, key=lambda x: x.name)
#         logger.info(f'sorting category: {category.name}')
#         logger.info([c.name for c in channels])
#         for channel in channels:
#             logger.info(channel.name)
#             await channel.edit(position=channels.index(channel))
#             await asyncio.sleep(2)
#         logger.info(f'done sorting category: {category.name}}')
#         await ctx.send(f'`done sorting category: {category.name}`')
#     await ctx.send('`finished sorting!`')
#

from sdxl_discord import SDXL
from discord.utils import get as discord_get


@bot.event
async def on_ready():
    task_wrapper["task"] = bot.loop.create_task(status_loop())

    # await bot.tree.sync()
    await bot.add_cog(UserCommands(bot))
    await bot.add_cog(SDXL(bot))
    await bot.tree.sync()
    # await test_cogs.setup(bot)
    logger.info("Bot started.")
    logger.info("~~~~~~~~~~~~~~~~~~~~~~~~")


if __name__ == "__main__":
    logger.info(f"discord version {discord.__version__}")
    with open("tokens.json", "r") as tokens_file:
        tokens = json.load(tokens_file)

    bot.run(tokens["secret"])
