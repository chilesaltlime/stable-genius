
def unpack_chain(chain):
    '''unpack the chain of messages into a single string'''
    unpacked = ''

    for message in chain[::-1]:
        if message is not None:
            unpacked += f'{message.author.name}: {message.content} \n'
    return str(unpacked)


async def get_message_chain(message):
    '''get the chain of messages that led to this message'''
    chain = [message]
    i = 0
    while message is not None and message.reference is not None:
        i += 1
        if i > 10:
            break
        # get message by its id
        message = await message.channel.fetch_message(message.reference.message_id)
        print(message.content)

        chain.append(message)
    return chain


                     