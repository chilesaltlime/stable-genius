
import openai
import os

from time import sleep
# show models
# print(openai.Engine.list())


def read_file_to_env():
    with open('openai_key.txt', 'r') as f:
        lines = f.readlines()
        for line in lines:
            key, value = line.split('=')
            os.environ[key] = value.strip()


async def get_response(prompt, max_tokens=196, engine='text-davinci-003'):
    await openai.Completion.acreate(
        engine=engine,
        prompt=prompt,
        temperature=0.95,
        max_tokens=max_tokens,
        top_p=1,
        stop=[".\n"],
        frequency_penalty=1.5,
        presence_penalty=1,
    )


def chat_message(prompt='hello', max_tokens=196, engine='text-curie-001'):
    # text-davinci-003

    def get_response(prompt, max_tokens):
        response = openai.Completion.create(
            engine=engine,
            prompt=prompt,
            temperature=0.95,
            max_tokens=max_tokens,
            top_p=1,
            stop=[".\n"],
            frequency_penalty=1.5,
            presence_penalty=1,
        )
        return response

    i = 0
    response = None
    while i < 5:
        i += 1
        try:
            response = get_response(prompt, max_tokens)
            if response:
                break
        except openai.error.RateLimitError:
            print(f'openai api error - retrying {i}')
            sleep(1)
            response = get_response(
                prompt, max_tokens, engine='text-curie-001')
    print(prompt)
    print(response)
    return response['choices'][0]['text'], response['usage']['total_tokens']


read_file_to_env()
openai.organization = os.environ.get('OPENAI_ORG_KEY')
openai.api_key = os.environ.get('OPENAI_API_KEY')
