import pprint
import openai
import os
from logger_default import custom_logger
from time import sleep


def read_file_to_env():
    with open("openai_key.txt", "r") as f:
        lines = f.readlines()
        for line in lines:
            key, value = line.split("=")
            os.environ[key] = value.strip()


def pretty(s):
    return pprint.pformat(s)


async def reply_in_chat(prompt: list, user: int, temp: float = 1.1):
    logger.info(pretty(prompt[-1]))

    async def api_with_retry(prompt, user):
        for i in range(3):
            try:
                return await openai.ChatCompletion.acreate(
                    model="gpt-4",
                    messages=prompt,
                    user=str(user),
                    temperature=temp,
                )

            except openai.error.RateLimitError as e:
                logger.warning(e)
                sleep(3)

    _response = await api_with_retry(prompt, user)
    _response = _response["choices"][0]["message"]["content"]
    logger.info(pretty(_response))
    logger.debug(f"Temperature: {temp}")

    return _response[:1900]


def get_temp():
    import random

    mu = 1.1
    sigma = 0.25
    temp = round(random.gauss(mu, sigma), 2)

    temp = max(0.1, temp)
    temp = min(2, temp)
    return temp


read_file_to_env()
openai.organization = os.environ.get("OPENAI_ORG_KEY")
openai.api_key = os.environ.get("OPENAI_API_KEY")
logger = custom_logger(__name__).logger


async def chat_message(prompt="hello", max_tokens=196, engine="gpt-4-1106-preview"):
    async def get_response(prompt, max_tokens, engine=engine):
        response = await openai.Completion.acreate(
            engine=engine,
            prompt=prompt,
            temperature=0.9,
            max_tokens=max_tokens,
            top_p=1,
            stop=[".\n"],
            frequency_penalty=1,
            presence_penalty=1.5,
            request_timeout=6,
        )
        return response

    response = None

    try:
        response = await get_response(prompt, max_tokens)

    except Exception as e:
        print(f"{e} - retrying")
        response = await get_response(prompt, max_tokens, engine="gpt-4-1106-preview")
    print(prompt)
    print(response)
    return response["choices"][0]["text"], response["usage"]["total_tokens"]

