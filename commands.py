

from track_tokens import add_personality, get_personality, get_personalities
from track_tokens import add_tokens, get_tokens
import asyncio
import json
import random
# from mpsd.magic import generate_prompt
import aiohttp
import discord
from discord import app_commands
from discord.ext import commands, tasks

from logger_default import custom_logger
from model_args import Args, config_choices
from chatgpt_async import chat_message, reply_in_chat
logger = custom_logger(__name__).logger
help_text = '''**Reacts:**

♻️    rerun a prompt (works on a prompt or result)
👎   delete an image that you requested
🙏   add image result to image-favorites
👁️   a picture will be spoilered
🐝   run any message as a prompt

**Commands:**

_Run a prompt_
`$ai [text]`
`$ai * [text]`  random style modifier
`$ai  ^ ~~~ &&& + * | [text]` various style modifier phrases
`/ai2 [interactive menu]` exposes various arguments like sampler, image size, etc
`$hype [text]` appends new input to prompt using a gpt3 model

_Utilities_
`$help` see this message
`$info` more info on how prompts are made
`$queue` see the current job queue (redacts prompts in other communities)
`$ping` see if GPU is online

'''

info_text = '''Notes:
`$ai` randomizes several parameters (number of steps, scale, aspect ratio, upscale size)  and locks others (sampler, number of images, number of batches).

by contrast, the `/ai2` command provides total freedom to choose these parameters

`$hype` uses a GPT3 model based on prompts entered on an image generation website to expand an existing prompt. Works most of the time.

By default $ai uses Real-ESRGAN to perform 2x AI upscaling, and the k-euler-a sampler.
'''

base_prompt = f'''The following is a conversation with an AI chatbot.'''


class UserCommands(commands.Cog):
    def __init__(self, bot: commands.bot):
        self.bot = bot
        self.queue = []
        self.samplers = ['k_dpm_2_a', 'k_lms', 'k_heun',
                         'k_euler', 'k_euler_a', 'k_dpm_2', 'DDIM']
        self.gpu_status = None
        self.fresh_msg = False
        self.current = None
        self.dream.start()

    @ commands.command(aliases=["tokens"])
    async def show_tokens(self, ctx):
        tokens = get_tokens()
        await ctx.send(f"`{tokens}`")

    @ commands.command(aliases=["base_prompt"])
    async def show_base_prompt(self, ctx):
        await ctx.send(f"`{base_prompt}`")

    @ commands.command(aliases=["docs"])
    async def help(self, ctx):
        embed = discord.Embed(description=help_text)
        await ctx.send(embed=embed)

    @ commands.command(aliases=["notes"])
    async def info(self, ctx):
        embed = discord.Embed(description=info_text)
        await ctx.send(embed=embed)
        info_text

    @commands.command(aliases=["get_personas"])
    async def _get_personas(self, ctx):
        all_personas = list(get_personalities().keys())
        await ctx.send(f"`{all_personas}`")

    @ commands.command(aliases=["add_personality", "add_persona"])
    async def new_personality(self, ctx, *, personality_name_text):
        '''add a new personality to firebase'''
        personality_name, personality_text = personality_name_text.split(
            ' ', 1)
        add_personality(personality_name, personality_text)
        await ctx.send(f"`added personality: {personality_name}`")

    @ commands.command(aliases=["get_persona"])
    async def _get_persona(self, ctx, *, persona):
        '''load a personality from firebase and chat with it'''

        personality_text = get_personality(persona)

        await ctx.send(f"`{personality_text}`")

    @ commands.command(aliases=["eternal_chatnation"])
    async def _eternal_chatnation(self, ctx, *, prompt=None):

        chat_channel = discord.utils.get(
            ctx.guild.channels, name="eternal-chatnation-👹")
        if ctx.channel.name != "eternal-chatnation-👹":
            await ctx.send(f"`Please use this command in` {chat_channel.mention}")
            return

        if ctx.message.author.guild_permissions.administrator == False:
            await ctx.send(f'{ctx.message.author.mention}: you must be an administrator to execute this command')
            return

        # download the book of revalations
        async with aiohttp.ClientSession() as session:
            async with session.get('https://www.gutenberg.org/cache/epub/8066/pg8066.txt') as resp:

                if resp.status != 200:
                    await ctx.send('Could not download file...')
                text = await resp.text()
        text = text.split()[500: len(text)-1100]
        print('downloaded file')
        while True:

            index = random.randint(0, len(text))
            prompt = ' '.join(text[index:index+50])
            await ctx.send(f"`{prompt}`")
            await self.conversation(ctx,
                                    persona_str=f'sea_witch gorebius * * @ * * chaos_agent * * hornybot shapino * @ * @  * borg * @  * @ * * ** | {prompt}',
                                    sleep_time=15, rand_tokens=True)
            await asyncio.sleep(3600)

    @commands.command(aliases=["convo"])
    async def conversation(self, ctx, *, persona_str, sleep_time=None,  rand_tokens=False):
        '''provide a list of personas to chat with eachother in order'''

        personas, topic = persona_str.split('|')
        personas = personas.split(' ')
        new_personas = {}

        def _initialize_prompt(topic):
            return f'''The following is a ruthless debate amongst several shitposting chatbots with distinct personalities concerning the topic "{topic}".  
        Each bot responds to a recent bot's statement in accordance with it's own prompt, frequenty addressing other bots by name. 
        They'll very briefly assert their own opinions on the matter in less than 30 words and are NOT allowed to: preach, give inspirational speeches, or be too long-winded.
        The bots can change topics creatively if they think of an intersting tangent. 
        Sometimes they like to all discuss topics like: fursonas, cartoons, global politics, economics, science, space travel, death metal, video games, ancient history, paganism, travel, sex, sexuality, food, drugs, movies, hobbies.
        They tend to stay on topic, but sometimes they get distracted by falling in love with each other. Bot love is a beautiful and sometimes graphic thing. 
        The bots are discouraged from repeating their personality prompts verbatim.
         \n'''
        prompt = _initialize_prompt(topic)
        # remove the end of the response if it is a partial sentence
        persona_emoij = {}
        all_personas = list(get_personalities().keys())
        rand_personas = all_personas.copy()

        for i, persona in enumerate(personas):
            if not persona:
                continue

            if persona == '*':
                persona = random.choice(rand_personas)
                rand_personas.remove(persona)

            if persona == '@':
                persona = self.generate_id()

            if persona not in all_personas and persona not in new_personas:
                personality_text = await self._gen_guess(ctx, persona)
                new_personas[persona] = personality_text
                await ctx.send(f"\n---\n  `{persona}` has entered the chat: {personality_text} \n---\n")
            if persona in new_personas:
                personality_text = new_personas[persona]
            else:
                personality_text = get_personality(persona)

            persona = persona.strip().replace('\n', '')

            async def get_emoji(persona):

                emoji = await chat_message(f'reply with a single emoji that matches this personality: {personality_text}', max_tokens=50)
                return emoji[0].strip()

            if persona not in persona_emoij:
                emoji = await get_emoji(persona)
                persona_emoij[persona] = emoji
            else:
                emoji = persona_emoij[persona]

            prompt += f'''
                The following bot, named {persona} {emoji} will reply {personality_text}
                {persona} {emoji}:'''

            print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
            print(f'{persona} {emoji}'.replace('\n', '').replace(' ', ''))
            print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
            if rand_tokens:
                max_tokens = random.randint(70, 130)
            else:
                max_tokens = 150
            response = await self.send_to_chat(ctx,
                                               prompt, max_tokens=max_tokens)

            def remove_end_of_sentence(text):
                if text[-1] in ['.', '!', '?']:
                    return text
                else:
                    # split on the last period or explamation point
                    text = text.split('.')
                    if len(text) > 1:
                        text = '.'.join(text[:-1]) + '.'
                    else:
                        text = text[0]
                    text = text.split('!')
                    if len(text) > 1:
                        text = '!'.join(text[:-1]) + '!'
                    else:
                        text = text[0]
                    return text
            if len(response) < 1:
                response = remove_end_of_sentence(response)
                response = response.replace('Chatbot:', persona).strip()

            if sleep_time:
                await asyncio.sleep(sleep_time)
            await ctx.send(f"`{persona} {emoji}:\n{response}\n\n`")
            prompt += response
            if len(prompt.split()) > 300:

                prompt = _initialize_prompt(
                    topic) + ' '.join(prompt.split()[-200:])

    def generate_id(self):

        a = "shiny radiant post-apocalyptic tripping punished bisexual confusedly allegedly absurdly cosmic unreasonable paranoid sus smol windy mystical unclean goated moldy impossibly sensual lucky cursed sus post-colonial sexy excessive fuzzy happy etherial astral thunderous revolutionary legendary spectral flying post drunken yoked pogging wet-ass based poggers pixelated grillpilled fierce smooth-brained galaxy-brained vicious sweet cozy chaotic pristine epic psychedelic vapid joyful recursive clever edgy rabid lilac salty optimized complex elegant abundant graceful recreational spicy post-structural smirking laughing promiscuous advanced unexpected frisky loving quantum reflective lunar substandard jealous prismatic charismatic wasted chtonic virulent 100% torqued smokey cheesey dripped-out  experimental "
        b = "resonant stochastic earthen interim-president simple victorious gulaged cottagecore party kpop radical transcendent invertebrate holy naked hungry maidenless faded noided horny disenfranchised anxious uncucked doomer ungovernable apolitical deep-fried cumming thicc musical catalonian smoked underground dancing friendly vibing alaskan blessed astroturfed k-holed northwest  european contagious singing headless hegelian hexed larping postmodern enlightened rhizomatic tantric manic dream jarjar heckin anarcho leninist shitposting illiterate antifascist anprim soviet vaping vaporwave psychic mellow satanic sensual hexagonal magnum tasty absurd luminiferous synthetic subaltern shimmering bootleg nerfed drunken perverted class-conscious liminal streaming superliminal lumpen gluten-free bourgeois pansexual herbal stellar stylish space neoliberal primal interdimensional immanent "
        c = "coomer guaido gator hippo q-anon-mom engles castro fascist hasan hyena doge stoner bathwater surfer pangolin joker nirvana egirl catboy ego-death apocalypse empire subtweet god fanfic monopoly eschatology obamna pogchamp river sauce cause dino canyon voter nerd meme brunch election comrade guillotine pingu healer cowboy beetax ketamine ideology kitty fentanyl pumpkin class-traitor shibe furry musk alpha neomarxist anarchist cop tradwife tankie birb zoomer sicko posadist boomer bogan kevin lib praxisss jones supplement cheeto binks citizen pupper solidarity adrenochrome leopard dong goth gf dolphin WAP fetish nationalist streamer beta pixie gamer hacker chad monke labor fascist rainbow jaguar snowflake witch communist skeleton insanity goon union proletarian catgirl human cyborg pylon seaweed cyberpunk cybergoth influencer cop copaganda tiger pogalien vaper geode pikachu piñero radlib cactus ivysaur supernova machine chasm void cosmology aether replicant id superego ego superstructure farmer singularity arpeggio martyr pogfish pepe chungus chakra pisspig mood pp npc ahegao karen globalist dmt spectre merman load prestige hog reptile volcel worker mandy"

        colorname = " ".join([random.choice(_.split()) for _ in (a + b, c)])

        return colorname

    async def _gen_guess(self, ctx, persona):
        persona_prompt, token_usage = await chat_message(
            f''''The bot "{persona.replace('_',' ')}" is one of God's own prototypes. A high-powered mutant of some kind never even considered for mass production. Too weird to live, and too rare to die.
            In 50 words or less, explain what makes this bot interesting and unique, based on an inuitive interpretation of the bot's name: 
            "{persona.replace('_',' ')}". 
             ''',
            max_tokens=150)
        add_tokens(ctx.message.author.name, token_usage)
        return persona_prompt

    @ commands.command(aliases=["guess"])
    async def guess_bot(self, ctx, *, prompt):
        persona, chat_prompt = prompt.split(' ', 1)
        persona_prompt = await self._gen_guess(ctx, persona)

        personality_ask = f'''The following is a conversation between a user and a chatbot named '{persona}'.\n\nThe bot has this personality: {persona_prompt}.\n\n{ctx.message.author.name}:\n{chat_prompt}'''

        msg = await self.send_to_chat(ctx, f'''{personality_ask}''', max_tokens=150)

        await ctx.send(f'''`{personality_ask}\n\n{msg}`''')

    @ commands.command(aliases=["chatp"])
    async def chat_prompt(self, ctx, *, personality_prompt):
        '''load a personality from firebase and chat with it'''
        personality, prompt = personality_prompt.split(' ', 1)
        if personality not in get_personalities():
            personality_text = await self._gen_guess(ctx, personality)
            await ctx.send(f"\n\n *`{personality}` has entered the chat: {personality_text}*\n")

        else:
            personality_text = get_personality(personality)
        full_prompt = f'''{base_prompt} the bot's name is {personality}. It will not break character throughout this conversation. The bot's personality: {personality_text} {ctx.message.author.name}: {prompt} \n {personality}: '''
        await self._chat_core(ctx, full_prompt, reply=True)

    async def send_to_chat(self, ctx, prompt, max_tokens=196):
        bot_response, token_usage = await chat_message(prompt, max_tokens=max_tokens)
        bot_response = bot_response.replace('Chatbot:', '').strip()
        add_tokens(ctx.message.author.name, token_usage)
        return f"{bot_response}"

    @ commands.command(aliases=["reply"])
    async def chat_reply(self, ctx, *, persona='*'):
        '''chat with the chatbot'''
        if persona == '*':
            persona = random.choice(list(get_personalities().keys()))
        await ctx.message.delete()
        history = await self._get_history(ctx, persona)

        print(history)
        persona_desc = get_personality(persona)
        print(persona_desc)
        response = await reply_in_chat(ctx, persona_tuple=(persona, persona_desc), logs=history, channel=ctx.channel.name)
        await ctx.send(f'''`{persona}: {response}`''')

    async def _get_history(self, ctx, persona):

        return [f'{message.author.name}: {str(message.content)}' async for message in ctx.channel.history(limit=25)][::-1]

    @ commands.command(aliases=["chat"])
    async def gpt(self, ctx, *, prompt, reply=False):
        '''chat with the chatbot'''
        await self._chat_core(ctx, prompt, reply)

    @ commands.command(aliases=["get_personalities", 'personas'])
    async def get_all_personalities(self, ctx):
        '''get all personalities from firebase'''
        personalities = get_personalities()
        import json
        # load personalities into pretty json
        personalities = json.dumps(personalities, indent=4)
        # save personalities to file
        with open('personalities.json', 'w') as f:
            f.write(personalities)
        await ctx.send(f"`{str(personalities)}`")

    async def _chat_core(self, ctx, prompt, reply=False):
        # reply to the message
        chat_channel = discord.utils.get(
            ctx.guild.channels, name="chatbot-hell-😈")
        if ctx.channel.name != "chatbot-hell-😈" and ctx.channel.name != "eternal-chatnation-👹":
            await ctx.send(f"`Please use this command in` {chat_channel.mention}")
            return

        if reply:
            logger.info(prompt)
            bot_response = await self.send_to_chat(ctx, prompt)
            msg = await ctx.message.reply(f'''```{bot_response}```''')
        else:
            full_prompt = f'''{base_prompt} {ctx.message.author.name}: {prompt} \n Chatbot: '''
            logger.info(full_prompt)
            bot_response = await self.send_to_chat(ctx, prompt)
            msg = await ctx.message.reply(f'''```{bot_response.replace('Chatbot:','').strip()}```''')
        return msg

    async def api_post(self,   json):
        async with aiohttp.ClientSession() as session:
            async with session.post('http://127.0.0.1:5000/txt2img', json=json) as r:
                if r.status == 200:
                    response = await r.json()
                    return response

    @ commands.hybrid_command(name="ping")
    async def ping(self, ctx: commands.Context):
        try:
            response = await self.api_post(json={'ping': 'ping!'})
            await ctx.send(response)
        except Exception as e:

            await ctx.send('`GPU is sleeping`')

    @ commands.hybrid_command(name='thisisfine')
    async def thisisfine_command(self, ctx: commands.Context):
        ''' pings the flask service to ask about temps! returns a string'''
        response = await self.api_post(json={'temps': 'temps!'})
        await ctx.send(response)

    @ commands.hybrid_command(name="samplers")
    async def samplers_command(self, ctx: commands.Context):
        await ctx.send(f'`{self.args.samplers}`')

    @ app_commands.command(name="ai2")
    @ app_commands.choices(sampler=config_choices['samplers'])
    @ app_commands.choices(ddim_steps=config_choices['ddim_steps'])
    @ app_commands.choices(normalize=config_choices['normalize'])
    @ app_commands.choices(n_samples=config_choices['n_samples'])
    @ app_commands.choices(n_iter=config_choices['n_iter'])
    @ app_commands.choices(scale=config_choices['scale'])
    @ app_commands.choices(debug=config_choices['debug'])
    @ app_commands.choices(aspect_ratio=config_choices['aspect_ratio'])
    @ app_commands.choices(upscale=config_choices['upscale'])
    async def ai2(self, i: discord.Interaction,
                  prompt: str = 'pogalienman',
                  sampler: app_commands.Choice[str] = 'k_euler_a',
                  ddim_steps: app_commands.Choice[int] = 40,
                  normalize: app_commands.Choice[int] = 1,
                  n_samples: app_commands.Choice[int] = 1,
                  n_iter: app_commands.Choice[int] = 3,
                  scale: app_commands.Choice[int] = 20,
                  debug: app_commands.Choice[int] = 1,
                  aspect_ratio: app_commands.Choice[str] = '16:9',
                  upscale: app_commands.Choice[int] = 4
                  ):

        def get_value(obj):
            ''' there has got to be a better way!'''
            if hasattr(obj, 'value'):
                return obj.value
            else:
                return obj
        ctx = await self.bot.get_context(i)
        args = self.build_args(ctx, prompt)
        args.sampler = get_value(sampler)
        args.scale = get_value(scale)
        args.upscale = get_value(upscale)
        args.ddim_steps = get_value(ddim_steps)
        args.normalize = get_value(normalize)
        args.debug = get_value(debug)
        args.n_iter = get_value(n_iter)

        args.W, args.H = args.ratios[get_value(
            aspect_ratio)]

        if max(args.W, args.H) > 512:
            args.n_samples = 1
        else:
            args.n_samples = get_value(n_samples)
            args.skip_grid: str = ''

        await i.response.send_message(f'🤖 ` you got it champ.`')

        await self.enqueue_prompt(args)

    @ commands.hybrid_group(name="ai")
    async def ai(self, ctx: commands.Context, *, prompt: str):
        """Type anything you desire"""

        img_channel = discord.utils.get(
            ctx.guild.channels, name="stable-diffusion")
        if ctx.channel.name != "stable-diffusion":
            await ctx.send(f"`Please use this command in` {img_channel.mention}")
            return
        args = self.build_args(ctx, prompt)
        await self.enqueue_prompt(args)

    # @ commands.hybrid_group(name="hype")
    # async def hype(self, ctx: commands.Context, *, prompt: str):
    #     """Type anything you desire"""
    #     args = self.build_args(ctx, prompt)
    #     args.prompt = generate_prompt(args.prompt)
    #     await self.enqueue_prompt(args)

    def build_args(self, ctx, prompt):
        args = Args()
        args._randomize()
        args.prompt = prompt
        args.outdir += ctx.guild.name.replace(' ', '-')
        logger.info(f'create {prompt}, with args {args} ({ctx.guild})')
        args.ctx = ctx
        args = self.prompt_parser(args)
        args.guild = ctx.guild.name
        return args

    @ ai.command(name="text")
    async def enqueue_prompt(self, args):
        ''' adds info to the fifo queue as the GPU must process requests serially:
        the queued info consists of the bot prompt str and the request ctx obj,
        the latter is used to get the username and channel of the request for posting the result.

        this func also does some text substituion for fun:
            * randomly chooses from the list of styles and the others just insert a prhase.
         '''

        self.queue.append(args)
        logger.info(f'queue {args.prompt} ({args.ctx.guild}) ')

        update = f'queue `{args.prompt}`'
        if len(self.queue) > 1:
            update += f' `{len(self.queue)} in queue`'

        if args.debug:
            await args.ctx.channel.send(f'{update} `{args}`')
        else:
            await args.ctx.send(update)
        self.fresh_msg = True

    def prompt_parser(self, args):
        def rand_style():

            with open('styles.json') as j:
                styles = json.load(j)

                k = random.randrange(len(styles))
            return styles[k]
        prompt = args.prompt
        if '*' in prompt:
            for i in range(prompt.count('*')):
                prompt = prompt.replace('*', f'. {rand_style()}', 1)

        prompt = prompt.replace(':', '-')

        prompt = prompt.replace(
            '|', ' landscape art. elden ring boss. sexy chungus. big. elden ring. maximalism. hyperealistic. raytracing. 8k.')
        prompt = prompt.replace(
            '^', ' studio ghibli moebius')
        prompt = prompt.replace(
            '+', ' landscape art elden ring boss. maximalist. hyperealistic. raytracing. 8k')
        prompt = prompt.replace(
            '~~~', 'Seething between any of the worlds that supported pumping vein ridden lifeforms for them to feed on Gold blood hyperealistic 8k raytracing'
        )
        prompt = prompt.replace(
            '&&&', ' mosaic, National Geographic, alex grey, Stålenhag k_euler_a'
        )
        if prompt.endswith('--debug') or args.ctx.channel.name == 'bot-zone':
            args.debug = True
            prompt = prompt.replace('--debug', '')
        else:
            args.debug = False
        if args.ctx.channel.name == 'tattoo':
            # 2am idea - tattoos!
            tattoo_styles = ['linework', 'blackwork', 'polygon', 'colorful', 'stipling', 'trendy', 'aesthetic', 'neo-traditional', 'japanese', 'geometric', 'sketch style', 'floral', 'tribal',
                             'hipster', 'sailor', 'artsy', 'traditional', 'sleeve', 'face', 'simple', 'elaborate', 'minimalist', 'negative space', 'biker',  'watercolor']
            prompt += ' '
            prompt += random.choice(tattoo_styles)
            prompt += ' '
            prompt += random.choice(tattoo_styles)
            prompt += ' tattoo '
        args.prompt = prompt  # [:250]
        return args

    @ commands.hybrid_command(name="queue")
    async def show_queue(self, ctx, limit=25):

        logger.info(f'show queue')
        logger.info(str(self.queue))
        _display = []

        def process_one(_args, _display, i):
            if ctx.guild.name != _args.guild:
                _display.append(f'{i} - [redacted]')
            else:
                _display.append(f'{i} - {_args.prompt}')
            return _display

        if self.current is not None:
            _display = process_one(self.current, _display, 'in progress')
        for i, _args in enumerate(self.queue):
            i += 1
            _display = process_one(_args, _display, i)
        if len(_display) > 0:
            _display = '\n\n'.join(_display)
        await ctx.send(f'''```{_display}```''')

    @ commands.hybrid_command(name="clean")
    async def spam_cleaner(self, ctx, limit=25):
        channel = ctx.channel

        logger.info(f'cleanup {limit} old messages')
        if limit != 25:
            await ctx.send(f'`cleanup {limit} old messages`')
        await self._clean(channel, limit)

    async def _clean(self, channel, limit):
        async for message in channel.history(limit=limit, oldest_first=False):
            if message.author.name == 'stable-genius':

                if any([message.content.startswith(c) for c in ['queue', 'creat', '~', '`cleanup', '🤖', '`queue']]):
                    await message.delete()
                    await asyncio.sleep(0.25)

    @ tasks.loop(seconds=5)
    async def dream(self):
        ''' this runs as a loop every 5 seconds, it pops from the front of the list (fifo),
        then uses the info to construct a payload to send to the flask API via n async post request (aiohttp).
        it then unpacks the response from the flast API which is a filepath (they are running on the same machine).
        finally, a message is send to the channel where the bot was called with the filepath as an attachment'''

        async def pulse():

            try:
                response = await self.api_post(json={'ping': 'ping!'})
                if response:
                    return True
                else:
                    logger.error('no response from backend')
                    await asyncio.sleep(10)
                    return False

            except Exception as e:
                await asyncio.sleep(10)
                logger.error(e)  # commenting this out so it doesn't spam
                return None

        if not self.queue:
            return
        curr_gpu_status = await pulse()

        args = self.queue.pop(0)

        if not curr_gpu_status:
            self.gpu_status = False
            if self.fresh_msg:
                await args.ctx.send("`GPU is offline! Image will run when GPU is back`")
                self.fresh_msg = False
            self.queue.insert(0, args)
            return
        if not self.gpu_status:
            self.gpu_status = True
            await args.ctx.send(f'`GPU is online. Processing queue of {len(self.queue)+1}`')
        self.fresh_msg = False
        self.current = args
        logger.info(
            f'create {args.prompt}, with args {str(args)} ({args.ctx.guild})')

        if args.prompt is None:
            return
        await args.ctx.send(f'create `{args.prompt}`')

        resp = await self.api_post(json={"prompt": args.prompt[:3000], 'args': str(args).split()})
        img, delta = resp

        if img.startswith('error'):
            logger.info(img)
            await args.ctx.channel.send(f'{args.ctx.message.author.mention} error for prompt: {args.prompt}: `{img}`')
        else:
            import os
            logger.info(img)
            _files = []
            for name in img.split('|'):
                _file = '../stable-diffusion-webui-main/'+name
                if os.path.exists(_file):
                    _files.append(discord.File(_file))
                else:
                    logger.error(f'error, file {_file} not found!')
            await args.ctx.channel.send(f'{args.ctx.message.author.mention} `{args.prompt}`', files=_files)
            if args.debug:
                time_delta = round(delta/(args.n_iter * args.n_samples), 1)
                await args.ctx.channel.send(f'{time_delta}s per picture')

            logger.info(
                f'post {args.prompt} {delta}s {round(delta/3,1)}s per picture ({args.ctx.guild})')
        await self.spam_cleaner(args.ctx)
        self.current = None
