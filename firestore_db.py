from google.cloud import firestore


import os

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "service-acct.json"

db = firestore.Client()

import pickle
from dataclasses import dataclass, asdict


from logger_default import custom_logger

logger = custom_logger(__name__).logger

from discord.utils import get as discord_get


@dataclass
class ContextWrapper:
    message_id: int
    channel_id: int

    @property
    def values(self):
        return self.__dict__

    async def get_ctx(self, bot):
        channel = bot.get_channel(self.channel_id)
        message = await channel.fetch_message(self.message_id)
        ctx = await bot.get_context(message)
        ctx.author = message.author
        return ctx


class FirestoreInterface:
    def __init__(self, bot):
        self.bot = bot

    async def get_all_ctx(self):
        return [await ctx_wrapper.get_ctx(self.bot) for ctx_wrapper in self.contexts]

    def add_messages(self, context_list):
        for ctx in context_list:
            self.add_msg({"message_id": ctx.message.id, "channel_id": ctx.channel.id})

    def add_msg(self, ctx_dict):
        doc_ref = db.collection("prompts").document("prompts")

        doc_ref.set({"prompts": firestore.ArrayUnion([ctx_dict])}, merge=True)
        logger.info(f"added {ctx_dict}")

    @property
    def contexts(self):
        doc_ref = db.collection("prompts").document("prompts")
        doc = doc_ref.get()
        prompts = doc.to_dict()
        if prompts.get("prompts") is None:
            return []

        return [ContextWrapper(**p) for p in prompts["prompts"]]

    def del_msg(self, ctx):
        ctx_dict = ContextWrapper(ctx.message.id, ctx.channel.id).values
        doc_ref = db.collection("prompts").document("prompts")
        doc_ref.update({"prompts": firestore.ArrayRemove([ctx_dict])})
        logger.info(f"deleted {ctx_dict}")

    @staticmethod
    def del_prompts():
        doc_ref = db.collection("prompts").document("prompts")
        doc_ref.update({"prompts": firestore.DELETE_FIELD})


class MockBot:
    def get_message(self, message_id):
        return f"message_id {message_id}"

    def get_context(self, message):
        return f"message {message}"


import types


def test_firestore_interface():
    def get_mock_ctx():
        import random

        ctx = types.SimpleNamespace()
        ctx.message = types.SimpleNamespace()
        ctx.channel = types.SimpleNamespace()
        ctx.message.id = random.randint(0, 100)
        ctx.channel.id = random.randint(0, 100)
        return ctx

    FirestoreInterface.del_prompts()


if __name__ == "__main__":
    test_firestore_interface()
