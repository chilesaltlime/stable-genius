from dataclasses import dataclass
import os
import json


class Gradio:
    def __init__(self):
        self.read_config()

    def read_config(self):
        self.config = json.load(open("gradio.json"))
        self.gradio_user, self.gradio_pass = self.config["creds"]
        os.environ["GRADIO_URL"] = self.config["url"]

    @property
    def base_url(self):
        url = os.environ["GRADIO_URL"]
        return f"https://{url}.gradio.live"
