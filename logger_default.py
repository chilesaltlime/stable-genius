import logging


class custom_logger:
    ''''a little logger that includes useful info like function and line no
        in addition to the regular stuff'''
    def __init__(self, name) -> None:
        self.name = name
        # create logger
        self.logger = logging.getLogger(self.name)
        self.logger.setLevel(logging.INFO)

        # create console handler and set level to info
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)

        # create formatter
        formatter = logging.Formatter(
            '%(levelname)s - %(name)s - %(funcName)s - %(lineno)d - %(message)s')

        # add formatter to ch
        ch.setFormatter(formatter)

        # add ch to logger
        self.logger.addHandler(ch)


if __name__ == '__main__':
    logger = custom_logger(__name__)
