import discord
from discord.ext import commands

###############
# This dictionary will store user responses
responses = {}
intents = discord.Intents.all()
bot = commands.Bot(command_prefix="%", intents=intents)


class RunPrompt:
    async def process_responses(user_response, user):
        prompt = user_response["prompt"]
        if user_response["use_gpt"] == "Yes":
            user_response["prompt"] += " -gpt "
        if user_response["neg_prompt"]:
            user_response["prompt"] += "-n " + user_response["neg_prompt"]
        ctx = RunPrompt.ctx
        ctx.message.content = prompt
        ctx.message.author = user
        hw = ratios[user_response["aspect_ratio"]]

        RunPrompt.hw = hw
        RunPrompt.prompt = prompt


ratios = {
    "3:4": (1280, 1024),
    "4:3": (1024, 1280),
    "16:9": (1408, 768),
    "9:16": (768, 1408),
    "1:1": (1024, 1024),
    "3:2": (1536, 1024),
    "2:3": (1024, 1536),
    "3:1": (1920, 640),
    "1:3": (640, 1920),
}


class Questionnaire(discord.ui.Modal, title="Questionnaire Response"):
    prompt = discord.ui.TextInput(
        label="Prompt",
        default="cosmic egg * * * *",
        style=discord.TextStyle.paragraph,
    )
    neg_prompt = discord.ui.TextInput(
        label="Negative Prompt",
        default="",
        style=discord.TextStyle.paragraph,
        required=False,
    )

    async def on_submit(self, interaction: discord.Interaction):
        user_id = interaction.user.id
        user_responses = responses.get(user_id, {})

        responses[user_id] = {
            "prompt": self.prompt.value,
            "neg_prompt": self.neg_prompt.value,
            "aspect_ratio": user_responses.get(
                "aspect_ratios", responses["defaults"]["aspect_ratios"]
            ),
            "use_gpt": user_responses.get("use_gpt", responses["defaults"]["use_gpt"]),
        }

        await interaction.response.defer()

        await interaction.delete_original_response()

        await RunPrompt.process_responses(responses[user_id], interaction.user)
        msg = await RunPrompt.ctx.send(
            f"{interaction.user.mention} Running prompt for `{RunPrompt.prompt}`"
        )

        await RunPrompt.bot.get_cog("SDXL").sdxl_img(RunPrompt.ctx, hw=RunPrompt.hw)
        await msg.delete()


class SubmitButton(discord.ui.Button):
    async def callback(self, interaction: discord.Interaction):
        questionnaire = Questionnaire()
        await interaction.response.send_modal(questionnaire)


class Dropdown(discord.ui.Select):
    def __init__(self, placeholder, options, custom_id):
        super().__init__(
            placeholder=placeholder,
            min_values=1,
            max_values=1,
            options=options,
            custom_id=custom_id,
        )

    async def callback(self, interaction: discord.Interaction):
        # Store the selection based on the dropdown's custom ID
        user_responses = responses.get(interaction.user.id, {}).copy()

        user_responses[self.custom_id] = self.values[0]

        responses[interaction.user.id] = user_responses

        await interaction.response.defer()


class DropdownView(discord.ui.View):
    def __init__(self, ctx, bot):
        super().__init__()
        self.ctx = ctx
        self.bot = bot
        RunPrompt.bot = bot
        RunPrompt.ctx = ctx

        aspect_ratios = [
            discord.SelectOption(label=k, description=str(v), emoji="📐")
            for k, v in ratios.items()
        ]

        aspect_ratios[0].default = True

        self.add_item(
            Dropdown(
                placeholder="Choose your aspect ratio",
                options=aspect_ratios,
                custom_id="aspect_ratios",
            )
        )

        use_gpt = [
            discord.SelectOption(
                label="Use GPT to embellish your prompt",
                description="Use GPT",
                emoji="🤖",
            ),
            discord.SelectOption(
                label="Don't send prompt to GPT",
                description="Skip GPT",
                emoji="😊",
                default=True,
            ),
        ]

        self.add_item(
            Dropdown(
                placeholder="Send prompt to GPT", options=use_gpt, custom_id="use_gpt"
            )
        )

        responses["defaults"] = {
            "aspect_ratios": [_.label for _ in aspect_ratios if _.default][0],
            "use_gpt": [_.label for _ in use_gpt if _.default][0],
        }

        self.add_item(SubmitButton(label="Submit"))


#################


class Bot(commands.Bot):
    def __init__(self):
        intents = discord.Intents.default()
        intents.message_content = True

        super().__init__(
            command_prefix=commands.when_mentioned_or("$"), intents=intents
        )

    async def on_ready(self):
        print(f"Logged in as {self.user} (ID: {self.user.id})")
        print("------")


import json

if __name__ == "__main__":

    @bot.command()
    async def xxx(ctx):
        """
        Sends a message with our dropdown containing colours
        and instantiates the Questionnaire modal if a selection is made.
        """
        from modals import DropdownView

        # Create the view containing our dropdown
        view = DropdownView(ctx, bot)

        # Sending a message containing our view
        await ctx.send("Pick your aspect ratio:", view=view)

    with open("tokens.json", "r") as tokens_file:
        tokens = json.load(tokens_file)
    bot.run(tokens["secret_dev"])
