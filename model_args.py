from discord import app_commands
from dataclasses import dataclass, asdict

from random import randint, choice


@dataclass
class Args:

    # H: int = 384
    # W: int = 704
    W: int = 512
    H: int = 640
    seed: int = 0
    n_iter: int = 3
    n_samples: int = 1
    scale: int = 15
    ddim_steps: int = 30
    sampler: str = 'k_euler_a'
    outdir: str = '--outdir outputs/'
    upscale: int = 2
    skip_grid = True
    ctx = None
    debug = False
    prompt = "pogalienman"
    normalize = True
    ratios = {
        '4:3': (640, 512),
        '3:4': (512, 640),
        '16:9': (704, 384),
        '9:16': (384, 704),
        '1:1': (512, 512),
        '3:2': (768, 512),
        '2:3': (512, 768),
        '3:1': (960, 320),
    }
    guild = None

    def _randomize(self):
        self.seed = randint(69, 42069)
        self.scale = randint(5, 20)
        self.ddim_steps = choice([25, 30, 35, 40, 50, 60])
        self.W, self.H = self.ratios[choice(list(self.ratios.keys()))]
        self.sampler: str = choice(
            ['k_euler_a', 'k_dpm_2_a'])
        # self.sampler: str = 'k_euler_a'

    def __str__(self):
        _str = ' '.join([f'--{k} {v} ' for k, v in asdict(self).items()])
        if self.normalize:
            _str += ' --normalize'
        if self.skip_grid:
            _str += ' --skip_grid'
        return _str


@ dataclass
class Choices:
    samplers: tuple = ('k_dpm_2_a', 'k_euler_a',  'DDIM', 'k_euler')
    # 'k_lms', 'k_heun', 'plms' 'k_euler', 'k_dpm_2', #these ones are available, but they kinda suck!

    ddim_steps: int = (10, 16, 25, 30, 40, 50, 60, 80, 120)
    normalize: tuple = (True, False)
    n_samples: tuple = (1, 2, 3)
    debug: tuple = (True, False)
    n_iter: tuple = (1, 2, 3, 4, 5, 6)
    scale: tuple = (5, 7, 10, 12, 20, 30)
    upscale: tuple = (1, 2, 4)
    aspect_ratio: tuple = ('3:4', '4:3', '16:9', '9:16',
                           '1:1', '3:2', '2:3', '3:1')

    ratios = {
        '3:4': (640, 512),
        '4:3': (512, 640),
        '16:9': (704, 384),
        '9:16': (384, 704),
        '1:1': (512, 512),
        '3:2': (768, 512),
        '2:3': (512, 768),
        '3:1': (960, 320),
    }

    def app_command(self, choices):

        return [
            app_commands.Choice(name=s, value=s) for s in choices
        ]

    def choice_dict(self):
        return {k: self.app_command(v) for k, v in asdict(self).items()}


config_choices = Choices().choice_dict()
