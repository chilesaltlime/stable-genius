import requests
from PIL import Image, PngImagePlugin
import io
import base64
import json
from logger_default import custom_logger
import aiohttp

from gradio_config import Gradio

gradio = Gradio()

logger = custom_logger(__name__).logger


async def get_args():
    return json.load(open("xl_args.json"))


async def process_img_api(
    prompt: str = "pikachu smoking weed",
    neg_prompt: str = "",
    model_type="SDXL",
    hw=None,
):
    prompt += ""

    im_args = json.load(open("xl_args.json"))
    payload = im_args

    if hw is not None:
        logger.error(hw)
        h, w = hw
        payload.update({"width": w, "height": h})

    payload["prompt"] = prompt + payload["prompt"]
    if len(neg_prompt) > 0:
        payload.update({"negative_prompt": neg_prompt})

    if "1.5" in model_type:
        payload.update({"width": 500, "height": 600})
    else:
        payload["prompt"] += " <lora:add-detail-xl:0.25> "

    logger.info(payload)
    async with aiohttp.ClientSession() as session:
        async with session.post(
            url=f"{gradio.base_url}/sdapi/v1/txt2img",
            json=payload,
            auth=aiohttp.BasicAuth(gradio.gradio_user, gradio.gradio_pass),
        ) as response:
            logger.info(dir(response))

            r = await response.json()

    for i in r["images"]:
        image = Image.open(io.BytesIO(base64.b64decode(i.split(",", 1)[0])))
        png_payload = {"image": "data:image/png;base64," + i}
        async with aiohttp.ClientSession() as session:
            async with session.post(
                url=f"{gradio.base_url}/sdapi/v1/png-info",
                json=png_payload,
                auth=aiohttp.BasicAuth("ocelot", "titties1234!@#$"),
            ) as r:
                png_response = await r.json()
                print(png_response)

        pnginfo = PngImagePlugin.PngInfo()
        pnginfo.add_text("parameters", png_response.get("info"))
        logger.info(png_response.get("info"))
        import hashlib

        image_hash = hashlib.md5(image.tobytes()).hexdigest()
        im_name = f"./images/{image_hash}.png"
        image.save(im_name, pnginfo=pnginfo)
        return im_name


if __name__ == "__main__":
    process_img_api()
