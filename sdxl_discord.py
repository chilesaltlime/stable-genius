import json
import random

import aiohttp
import discord
from discord.ext import commands, tasks

from logger_default import custom_logger
from sd_api import get_args, process_img_api

logger = custom_logger(__name__).logger

from discord.utils import get as discord_get

from firestore_db import FirestoreInterface


class ImageQueue:
    """Contains a dictionary with the key being the guild id and the value being a list of messages"""

    def __init__(self, bot):
        self.queue = []
        self.fs = FirestoreInterface(bot)
        self.bot = bot

    async def add(self, ctx):
        """add a message to the queue"""
        logger.info(
            f"adding message {ctx.message.content} in guild {ctx.guild.name} to queue"
        )

        self.queue.append(ctx)

    @tasks.loop(seconds=15)
    async def retry_sdxl_loop(self):
        """loop through the queue and retry the messages"""
        local_messages = self.queue
        stored_messages = self.fs.contexts

        def clean_content(_ctx):
            return _ctx.message.clean_content.replace("$xl", "").strip()

        if len(local_messages) or len(stored_messages):
            logger.info(f"firestore messages: {stored_messages}")
            logger.info(
                f"local messages: {[clean_content(ctx) for ctx in local_messages] }"
            )

        logger.debug("pinging backend")
        backend_up = await api_ping()

        if not backend_up:
            logger.info("backend not up")

            self.fs.add_messages(self.queue)
            self.queue = []
            return

        if len(local_messages) > 0:
            for _ctx in local_messages:
                await self.bot.get_cog("SDXL").sdxl_img(_ctx)

        if len(stored_messages) > 0:
            stored_messages = await self.fs.get_all_ctx()
            logger.info(f"found {len(stored_messages)} messages in firestore")
            channels = set([_ctx.channel for _ctx in stored_messages])
            for channel in channels:
                channel_messages = [
                    clean_content(_ctx)
                    for _ctx in stored_messages
                    if _ctx.channel.id == channel.id
                ]
                logger.info(f"`retrying queued messages: {channel_messages}`")
                await channel.send(f"`retrying queued messages: {channel_messages}`")

            for _ctx in stored_messages:
                logger.info(
                    f"found guild {_ctx.guild.name} with channel {_ctx.channel.name}"
                )
                logger.info(f"retrying prompt {_ctx.message}")
                await self.bot.get_cog("SDXL").sdxl_img(_ctx)
                self.fs.del_msg(_ctx)


class SDXL(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.image_queue = ImageQueue(bot)

        logger.info(f"starting retry loop")
        self.image_queue.retry_sdxl_loop.start()
        with open("styles.json", "r") as f:
            self.styles = json.load(f)
        self.model_name = ""

    @commands.command()
    async def show_queue(self, ctx):
        await ctx.send(f"`{ self.image_queue.queue}`")

    @commands.command(aliases=["xl", "sd", "poop"])
    async def sdxl_img(self, ctx, hw=None):
        # ping
        backend_up = await api_ping()
        if not backend_up:
            logger.debug("backend not up")
            await self.image_queue.add(ctx)
            return
        if "sdxl" in ctx.message.channel.name:
            """send a POST request to sd_api with a prompt"""
            logger.info("sending request to text2img")
            ctx.message.content = ctx.message.content.replace("$xl", "").strip()
            message = ctx.message.content

            try:
                logger.info(
                    f"attempting text2img with prompt {ctx.message.content} - {ctx.guild.name}"
                )
                _neg_prompt = ""
                if "-n" in ctx.message.content:
                    _prompt = ctx.message.content.split("-n")[0].strip()
                    _neg_prompt = ctx.message.content.split("-n")[1].strip()

                else:
                    _prompt = ctx.message.content

                logger.info(ctx.channel.name.lower())
                if not "horny" in ctx.channel.name.lower():
                    _neg_prompt += " nsfw"

                if "*" in _prompt:
                    for _ in _prompt:
                        if "*" in _:
                            # get random style
                            _style = random.choice(self.styles)
                            # add style to prompt
                            _prompt = f"{_prompt} {_style}"
                    _prompt = _prompt.replace("*", "").strip()
                logger.info(f"prompt: {_prompt}")
                sd_model = await self.xl_model(ctx, show_result=False)
                sd_model = self.model_type(sd_model)
                model_name = await self.xl_model(ctx, show_result=False)
                if model_name != self.model_name:
                    self.model_name = model_name
                    # await ctx.message.channel.send(f"using model: ```{model_name}```")

                # no more penis
                for p in ["penis", "dick", "peen", "cock", "phallus"]:
                    _prompt = _prompt.replace(p.lower(), "cactus")
                if "-gpt" in _prompt:
                    _prompt = ctx.message.content.split("-gpt")[0].strip()
                    from chatgpt_async import reply_in_chat

                    chat_prompt = [
                        {
                            "role": "system",
                            "content": f'{open("sdxl_prompt.txt", "r").read()}',
                        },
                        {"role": "user", "content": _prompt},
                    ]

                    _prompt = await reply_in_chat(
                        prompt=chat_prompt, user=ctx.message.author.id, temp=1.11
                    )
                try:
                    response = await process_img_api(
                        prompt=_prompt,
                        neg_prompt=_neg_prompt,
                        model_type=sd_model,
                        hw=hw,
                    )
                except Exception as e:
                    logger.error(e)
                    await ctx.message.channel.send(
                        f"{ctx.message.author.mention} `{_prompt} `error: {e}`"
                    )

                    return
                # embed image in discord message
                user_topic_str = f"{ctx.message.author.mention} `{_prompt}`"
                if _neg_prompt:
                    user_topic_str += f" `-n {_neg_prompt}`"

                await ctx.message.channel.send(
                    user_topic_str,
                    file=discord.File(response),
                )
            except Exception as e:
                # show traceback
                import traceback

                traceback.print_exc()

                if ctx.message.author.name != "stable-genius":
                    await ctx.message.channel.send(
                        f"{ctx.message.author.mention} `GPU is not running, will run when it's back`"
                    )
                await self.image_queue.add(ctx)

        else:
            await ctx.message.channel.send(
                f"{ctx.message.author.mention} `please use this command in the #sdxl channel`"
            )

    @commands.command()
    async def xl_model(self, ctx, show_result=True):
        url = "/sdapi/v1/options"

        data = await api_get(url)

        _model = data["sd_model_checkpoint"]
        # if this was called by a command and not directly, send the model
        if show_result:
            logger.warning(ctx.author.name)
            await ctx.send(f"```{_model}```")
        return _model

    @commands.command()
    async def xl_args(self, ctx):
        await ctx.send(f"```{await get_args()}```")

    def model_type(self, model: str):
        if "1.5" in model:
            _model = "SD 1.5"
        elif "xl" in model.lower():
            _model = "SDXL"
        else:
            _model = "SD 1.5"
        return _model

    @commands.command()
    async def xl_loras(self, ctx):
        """get a random lora"""
        logger.info("getting loras")

        _refresh = await api_post("/sdapi/v1/refresh-loras")
        logger.info(_refresh)
        response = await api_get("/sdapi/v1/loras")

        _valid_loras = []
        for _lora in response:
            _name = _lora["name"]
            _alias = f'<lora:{_lora["alias"]}:1>'
            try:
                _tags = _lora["metadata"]["ss_tag_frequency"]
            except KeyError:
                _tags = ""
            try:
                _model = _lora["metadata"]["ss_sd_model_name"]
            except KeyError:
                _model = ""
            _model_type = self.model_type(_model)
            sd_model = await self.xl_model(ctx, show_result=False)
            sd_model = self.model_type(sd_model)
            if sd_model != _model_type:
                continue

            _valid_loras.append(
                {
                    "name": _name,
                    "alias": _alias,
                    # "tags": list(_tags),
                    # "model_type": _model_type,
                }
            )
        await ctx.message.channel.send(f"```Current model: {sd_model}```")

        await ctx.message.channel.send(f"```{json.dumps(_valid_loras, indent=4)}```")


async def api_get(url):
    async with aiohttp.ClientSession() as session:
        async with session.get(
            gradio.base_url + url,
            auth=aiohttp.BasicAuth(gradio.gradio_user, gradio.gradio_pass),
        ) as r:
            if r.status == 200:
                response = await r.json()
                return response
            else:
                logger.error(f"error getting {url}: {r.status}")


from gradio_config import Gradio

gradio = Gradio()


# /sdapi/v1/refresh-loras
async def api_post(url, json=None):
    async with aiohttp.ClientSession() as session:
        full_url = gradio.base_url + url
        async with session.post(
            full_url,
            json=json,
            auth=aiohttp.BasicAuth(gradio.gradio_user, gradio.gradio_pass),
        ) as r:
            if r.status == 200:
                response = await r.json()
                return response


async def api_ping():
    timeout = aiohttp.ClientTimeout(total=5)
    status = False

    async with aiohttp.ClientSession() as session:
        logger.debug("pinging api")
        try:
            async with session.get(
                gradio.base_url + "/user/",
                timeout=timeout,
                auth=aiohttp.BasicAuth(gradio.gradio_user, gradio.gradio_pass),
            ) as r:
                status = r.status == 200
                logger.debug(f"api status: {status}")
        except:
            logger.warning(f"api not up - {r.status}")
            raise
        finally:
            return status
