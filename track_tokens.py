from unidecode import unidecode
from google.cloud import firestore
import pandas as pd
import os


# create env variable
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'service-acct.json'

db = firestore.Client()


def add_tokens(user, tokens):
    user = unidecode(user).replace('-', '')
    active = db.collection('tokens').document('user_spend')
    active.update({user: firestore.Increment(tokens)})


def get_tokens():
    ''' get total tokens spent '''

    active = db.collection('tokens').document('user_spend')

    # use pandas to read a dict into a dataframe
    usage = active.get().to_dict()
    print(usage)
    df = pd.DataFrame(columns=['user', 'tokens'])

    for user, tokens in usage.items():
        row = pd.DataFrame([{'user': user, 'tokens': tokens}],
                           columns=['user', 'tokens'])
        df = pd.concat([df, row], ignore_index=True)

    totals = pd.DataFrame([
        {'user': 'Total', 'tokens': df['tokens'].sum()}])
    df = pd.concat([df, totals], ignore_index=True)

    df['$'] = df['tokens'] * (0.02 / 1000)
    # convert the dollar amount to float and round to 2 decimal places
    df['$'] = df['$'].apply(lambda x: round(x, 3))
    df = df.sort_values(by=['tokens'], ascending=True)
    return df.to_markdown(index=False, tablefmt='pretty')


def add_personality(personality_name, personality_text):
    ''' add a personality to the database '''

    active = db.collection('personality').document('personality')
    active.set({personality_name: personality_text}, merge=True)


def remove_personality(personality_name):
    ''' remove a personality from the database '''

    active = db.collection('personality').document('personality')
    active.update({personality_name: firestore.DELETE_FIELD})
    print(f'{personality_name} removed from database')


def get_personalities():
    ''' get all personalities from the database '''

    active = db.collection('personality').document('personality')
    personalities = active.get().to_dict()
    # save personalities to a json file

    return personalities


def get_personality(personality_name):
    ''' get a personality from the database '''

    active = db.collection('personality').document('personality')
    return active.get().to_dict()[personality_name]


if __name__ == '__main__':

    print(get_tokens())
    remove_personality('musk,')

    print(list(get_personalities().keys()))
